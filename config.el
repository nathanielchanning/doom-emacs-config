;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name ""
      user-mail-address "")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
(setq doom-font (font-spec :family "monospace" :size 14))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
;; (setq doom-theme 'doom-one)
;; (setq doom-theme 'doom-gruvbox)
(setq doom-theme 'doom-monokai-pro)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/.org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type nil)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c g k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c g d') to jump to their definition and see how
;; they are implemented.
(after! evil-mode
  ;;(evil-define-key '(normal insert) lisp-mode-map "M-." 'slime-edit-definition)
  ;;(evil-define-key '(normal insert) lisp-mode-map "M-." 'sly-edit-definition)
  (setq evil-escape-key-sequence "jk"))

(after! company
  (defun insert-space ()
    (insert ?\ ))

  (defun vim-complete-candidate (candidates)
    "Emulates Vim's complete by placing the current prefix
as the first selection."
    (let ((prefix (company-grab-symbol)))
      (if (string= prefix (car candidates))
          candidates
          (push prefix candidates))))

  (define-key company-active-map (kbd "SPC") #'company-complete-selection)
  (advice-add 'company-complete-selection :after #'insert-space)
  ;; Add completion up to point on.
  (setq company-transformers (list #'vim-complete-candidate)))

(after! ivy
  (setq ivy-re-builders-alist '((t . ivy--regex-fuzzy))))

(use-package magit)
;;; (use-package slime)

;;; (after! slime
;;;   (setq inferior-lisp-program "/usr/bin/sbcl")
;;;   (setq slime-contribs '(slime-fancy slime-company)))

(use-package sly)

(after! sly
  (require 'sly-autoloads)
  (setq sly-contribs
        '(sly-fancy sly-named-readtables sly-asdf sly-quicklisp sly-macrostep))
  ;; This can be commented after starting once.
  (sly-setup)
  (setq inferior-lisp-program "/usr/bin/sbcl"))

(add-hook 'lisp-mode-hook (lambda () (setq fill-column 80)))

;;; Support for curry-compose-reader-macros
(modify-syntax-entry ?\[ "(]" lisp-mode-syntax-table)
(modify-syntax-entry ?\] ")[" lisp-mode-syntax-table)
(modify-syntax-entry ?\{ "(}" lisp-mode-syntax-table)
(modify-syntax-entry ?\} "){" lisp-mode-syntax-table)
(modify-syntax-entry ?\« "(»" lisp-mode-syntax-table)
(modify-syntax-entry ?\» ")«" lisp-mode-syntax-table)
(modify-syntax-entry ?\‹ "(›" lisp-mode-syntax-table)
(modify-syntax-entry ?\› ")‹" lisp-mode-syntax-table)
;;; TODO: figure out how to include the currently typed section in the
;;;       completion pop-up.

;;; evil undo support
;; (use-package undo-tree)
;; (global-undo-tree-mode t)
;; (evil-set-undo-system 'undo-tree)

;;; Save what's in the x clipboard before deleting.
(setq save-interprogram-paste-before-kill t)

(general-unbind 'normal "<" ">")

(map! :n "M-." #'sly-edit-definition
      :v "(" #'sp-wrap-round
      (:prefix ("<" . "sexp<")
       :n "(" #'sp-backward-barf-sexp
       :n ")" #'sp-forward-slurp-sexp)
      (:prefix (">" . "sexp>")
       :n ")" #'sp-forward-barf-sexp
       :n "(" #'sp-backward-slurp-sexp))

;;; Python
(setq python-shell-interpreter "python3")

;;; NOTE: usage notes:
;;;       - M-x pyvenv-activate ; activate the venv
;;;       - M-x dap-debug ; runs test at point. Can also run from eshell.
;;;       - M-x dap-breakpoint-toggle or SPC-dbb ; For break points.

(after! dap-mode
  ;; NOTE: pip install debugpy
  (setq dap-python-debugger 'debugpy)
  (setq dap-python-executable "python3")
  (map! :map dap-mode-map
        :leader
        :prefix ("d" . "dap")
        ;; basics
        :desc "dap next"          "n" #'dap-next
        :desc "dap step in"       "i" #'dap-step-in
        :desc "dap step out"      "o" #'dap-step-out
        :desc "dap continue"      "c" #'dap-continue
        :desc "dap hydra"         "h" #'dap-hydra
        :desc "dap debug restart" "r" #'dap-debug-restart
        :desc "dap debug"         "s" #'dap-debug

        ;; debug
        :prefix ("dd" . "Debug")
        :desc "dap debug recent"  "r" #'dap-debug-recent
        :desc "dap debug last"    "l" #'dap-debug-last

        ;; eval
        :prefix ("de" . "Eval")
        :desc "eval"                "e" #'dap-eval
        :desc "eval region"         "r" #'dap-eval-region
        :desc "eval thing at point" "s" #'dap-eval-thing-at-point
        :desc "add expression"      "a" #'dap-ui-expressions-add
        :desc "remove expression"   "d" #'dap-ui-expressions-remove

        :prefix ("db" . "Breakpoint")
        :desc "dap breakpoint toggle"      "b" #'dap-breakpoint-toggle
        :desc "dap breakpoint condition"   "c" #'dap-breakpoint-condition
        :desc "dap breakpoint hit count"   "h" #'dap-breakpoint-hit-condition
        :desc "dap breakpoint log message" "l" #'dap-breakpoint-log-message))

;;; Terminal support for dap-debug
(xterm-mouse-mode 1)
